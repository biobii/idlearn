let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.scripts([
			'public/js/ela/lib/jquery/jquery.min.js',
			'public/js/ela/lib/bootstrap/js/popper.min.js',
			'public/js/ela/lib/bootstrap/js/bootstrap.min.js',
			'public/js/ela/jquery.slimscroll.js',
			'public/js/ela/sidebarmenu.js',
			'public/js/ela/lib/sticky-kit-master/dist/sticky-kit.min.js',
			'public/js/ela/lib/owl-carousel/owl.carousel.min.js',
			'public/js/ela/lib/owl-carousel/owl.carousel-init.js',
			'public/js/ela/scripts.js',
			'public/js/ela/custom.min.js',
		],
		'public/js/ela.js')	
	.styles([
			'public/css/lib/calendar2/pignose.calendar.min.css',
			'public/css/lib/calendar2/semantic.ui.min.css',
			'public/css/lib/owl.theme.default.min.css',
			'public/css/lib/owl.carousel.min.css',
			'public/css/lib/bootstrap/bootstrap.min.css',
			'public/css/helper.css',
			'public/css/style.css',
			'public/css/custom.css',
		], 'public/css/ela.css')
	.styles([
			'public/icons/font-awesome/css/font-awesome.min.css',
			'public/icons/simple-line-icons/css/simple-line-icons.css',
			'public/icons/weather-icons/css/weather-icons.min.css',
			'public/icons/linea-icons/linea.css',
			'public/icons/themify-icons/themify-icons.css',
			'public/icons/flag-icon-css/flag-icon.min.css',
			'public/icons/material-design-iconic-font/css/materialdesignicons.min.css',
			'public/css/spinners.css',
			'public/css/animate.css',
		], 'public/css/ela-icons.css')
   .sass('resources/assets/sass/app.scss', 'public/css');
