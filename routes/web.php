<?php

Route::get('/', function () {
    return view('landing');
});

// authentication
Route::prefix('auth')->group(function () {
    Route::prefix('a')->group(function () {
        Route::get('/masuk', 'Auth\AdminLoginController@showLoginForm')
            ->name('admin.login');

        Route::post('/masuk', 'Auth\AdminLoginController@login')
            ->name('admin.login.submit');

        Route::get('/keluar', 'Auth\AdminLoginController@logout')
            ->name('admin.logout');

        Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')
            ->name('admin.password.email');

        Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')
            ->name('admin.password.request');

        Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');

        Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')
            ->name('admin.password.reset');
    });

    Route::prefix('d')->group(function () {
        Route::get('/masuk', 'Auth\LecturerLoginController@showLoginForm')
            ->name('lecturer.login');

        Route::post('/masuk', 'Auth\LecturerLoginController@login')
            ->name('lecturer.login.submit');

        Route::get('/keluar', 'Auth\LecturerLoginController@logout')
            ->name('lecturer.logout');

        Route::post('/password/email', 'Auth\LecturerForgotPasswordController@sendResetLinkEmail')
            ->name('lecturer.password.email');

        Route::get('/password/reset', 'Auth\LecturerForgotPasswordController@showLinkRequestForm')
            ->name('lecturer.password.request');

        Route::post('/password/reset', 'Auth\LecturerResetPasswordController@reset');

        Route::get('/password/reset/{token}', 'Auth\LecturerResetPasswordController@showResetForm')
            ->name('lecturer.password.reset');
    });

    Route::prefix('u')->group(function () {
        Route::get('/masuk', 'Auth\LoginController@showLoginForm')
            ->name('user.login');

        Route::post('/masuk', 'Auth\LoginController@login')
            ->name('user.login.submit');

        Route::get('/keluar', 'Auth\LoginController@logout')
            ->name('user.logout');

        Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
            ->name('user.password.email');

        Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
            ->name('user.password.request');

        Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

        Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
            ->name('password.reset');

        Route::get('/daftar', 'Auth\RegisterController@showRegisterForm')
            ->name('user.register');

        Route::post('/daftar', 'Auth\RegisterController@register')
            ->name('user.register.submit');
    });
});

Route::prefix('a')->group(function () {
    Route::get('/beranda', 'AdminController@index')
        ->name('admin.home');

    Route::get('/pengguna', 'AdminController@users')
        ->name('admin.users');
        
    Route::get('/lecturer', 'AdminController@lecturers')
        ->name('admin.lecturer');

    Route::delete('/hapus-kelas/{key}', 'AdminController@deleteClass')
        ->name('admin.deleteClassRoom');

    Route::get('/tambah-dosen', 'AdminController@addLecturer')
        ->name('admin.addLecturer');
        
    Route::post('/tambah-dosen', 'AdminController@storeLecturer') // via ajax
        ->name('admin.storeLecturer');

    Route::post('/hapus-dosen/{nip}', 'AdminController@deleteLecturer')
        ->name('admin.deleteLecturer');

    Route::post('/aktifkan-dosen/{nip}', 'AdminController@activateLecturer')
        ->name('admin.activateLecturer');

    Route::get('/edit-dosen/{nip}', 'AdminController@formEditLecturer')
        ->name('admin.formEditLecturer');

    Route::put('/edit-dosen/{nip}', 'AdminController@updateLecturer')
        ->name('admin.updateLecturer');

    Route::post('/hapus-user/{nipd}', 'AdminController@deleteUser')
        ->name('admin.deleteuser');

    Route::post('/aktifkan-user/{nipd}', 'AdminController@activateUser')
        ->name('admin.activateUser');

    Route::get('/edit-user/{nipd}', 'AdminController@formEditUser')
        ->name('admin.formEditUser');

    Route::put('/edit-user/{nipd}', 'AdminController@updateUser')
        ->name('admin.updateUser');

    Route::get('/profil', 'AdminController@showProfile')
        ->name('admin.showProfile');

    Route::put('/update-profile', 'AdminController@updateProfile')
        ->name('admin.updateProfile');
});

Route::prefix('d')->group(function () {
    Route::get('/beranda', 'LecturerController@index')
        ->name('lecturer.home');

    Route::get('/kelas-baru', 'LecturerController@createClassRoom')
        ->name('lecturer.createClassRoom');

    Route::post('/kelas-baru', 'LecturerController@storeClassRoom')
        ->name('lecturer.storeClassRoom');

    Route::get('/kelas-edit/{key}', 'LecturerController@editClassRoom')
        ->name('lecturer.editClassRoom');

    Route::put('/kelas-edit/{key}', 'LecturerController@updateClassRoom')
        ->name('lecturer.updateClassRoom');

    Route::delete('/kelas/{key}/hapus', 'LecturerController@deleteClassRoom')
        ->name('lecturer.deleteClassRoom');

    Route::get('/kelas/{key}', 'LecturerController@showClassRoom')
        ->name('lecturer.showDetailClassRoom');

    Route::post('/kelas/{key}/tambah-asisten/{nipd}', 'LecturerController@createAssistant')
        ->name('lecturer.addAssistant');

    Route::delete('/kelas/{key}/hapus-asisten/{nipd}', 'LecturerController@deleteAssistant')
        ->name('lecturer.deleteAssistant');

    Route::delete('/kelas/{key}/keluarkan/{nipd}', 'LecturerController@kickUser')
        ->name('lecturer.kickUser');

    Route::get('/kelas/{key}/materi-baru', 'LecturerController@createLecture')
        ->name('lecturer.createLecture');

    Route::post('/kelas/{key}/materi-baru', 'LecturerController@storeLecture')
        ->name('lecturer.storeLecture');

    Route::get('/profil', 'LecturerController@showProfile')
        ->name('lecturer.showProfile');

    Route::put('/profil', 'LecturerController@updateProfile')
        ->name('lecturer.updateProfile');
});

Route::prefix('u')->group(function () {
    Route::get('/beranda', 'UserController@index')
        ->name('user.home');

    Route::get('/kelas/{key}', 'UserController@showClass')
        ->name('user.class');

    Route::get('/kelas/{key}/materi', 'UserController@showAllLectures')
        ->name('user.class.lectures');

    Route::delete('/kelas/{key}/keluar', 'UserController@leaveClass')
        ->name('user.leaveClass');
    
    Route::get('/kelas/{key}/materi/{lecure_id}', 'UserController@detailLecture')
        ->name('user.class.lectures-detail');

    Route::get('/kelas/{key}/materi/{lecture_id}/{filename}', 'UserController@fileLecture')
        ->name('user.class.lectures-file');

    Route::get('/profil', 'UserController@profile')
        ->name('user.profile');

    Route::put('/profil', 'UserController@updateProfile')
        ->name('user.updateProfile');

    Route::get('/kelas/{key}/buat-soal', 'UserController@createQuestion')
        ->name('user.createQuestion');

    Route::get('/nilai/multiple', 'UserController@showMultipleValue')
        ->name('user.showMultipleValue');

    // via ajax
    Route::post('/gabung-kelas', 'UserController@joinClass')
        ->name('user.joinClass');

    Route::get('/data-kelas', 'UserController@followedClass')
        ->name('user.followedClass');

    Route::post('/soal/multiple', 'UserController@createMultipleQuestion')
        ->name('user.createMultipleQuestion');

    Route::get('/soal/multiple/{key}/{lecture_id}', 'UserController@getMultipleQuestion')
        ->name('user.getMultipleQuestion');

    Route::post('/soal/submit-answers-multiple', 'UserController@submitAnswersMultiple')
        ->name('user.submitAnswersMultiple');


});
