<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionMultiple extends Model
{
    
    protected $table = 'question_multiples';

    protected $fillable = [
        'user_nipd', 'class_key', 'lecture_id', 'question', 'choice_a', 'choice_b', 'choice_c', 'choice_d', 'key',
    ];

    /*-----------
     * Relation
     *-----------
     */

     // who was created question.
     public function user()
     {
        return $this->belongsTo('App\Models\User', 'user_nipd');
     }

     public function classroom()
     {
        return $this->belongsTo('App\Models\ClassRoom', 'class_key');
     }

     public function lecture()
     {
         return $this-belongsTo('App\Models\Lecture', 'lecture_id');
     }

}
