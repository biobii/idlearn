<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreMultipleQuestion extends Model
{
    
    protected $table = 'score_multiple_questions';

    protected $fillable = [
        'user_nipd', 'class_key', 'lecture_id', 'score',
    ];

    /*-----------
     * Relation
     *-----------
     */

     public function user()
     {
         return $this->belongsTo('App\Models\User', 'user_nipd');
     }

     public function classroom()
     {
         return $this->belongsTo('App\Models\ClassRoom', 'class_key');
     }

     public function lecture()
     {
         return $this->belongsTo('App\Models\Lecture', 'lecture_id');
     }

}
