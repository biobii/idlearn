<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerEssay extends Model
{
    
    protected $table = 'answer_essays';

    protected $fillable = [
        'question_essay_id', 'user_nipd', 'lecture_id', 'answer', 'point',
    ];

    /*-----------
     * Relation
     *-----------
     */

     public function questionEssay()
     {
         return $this->belongsTo('App\Models\QuestionEssay', 'question_essay_id');
     }

     // who was answer question.
     public function user()
     {
        return $this->belongsTo('App\Models\User', 'user_nipd');
     }

     public function lecture()
     {
         return $this->belongsTo('App\Models\Lecture', 'lecture_id');
     }

}
