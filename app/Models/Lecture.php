<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    
    protected $table = 'lectures';

    protected $fillable = [
        'lecturer_id', 'class_key', 'title', 'description', 'lecture_file',
    ];

    public function lecturer()
    {
        return $this->belongsTo('App\Models\Lecturer', 'lecturer_id');
    }

    public function classroom()
    {
        return $this->belongsTo('App\Models\ClassRoom', 'class_key');
    }

    public function questionMultiples()
    {
        return $this->hasMany('App\Models\QuestionMultiple', 'lecture_id', 'id');
    }

    public function questionEssays()
    {
        return $this->hasMany('App\Models\QuestionEssay', 'lecture_id', 'id');
    }

    public function answersEssays()
    {
        return $this->hasMany('App\Models\AnswerEssay', 'lecture_id', 'id');
    }

    public function scoreMultiple()
    {
        return $this->hasMany('App\Models\ScoreMultipleQuestion', 'lecture_id', 'id');
    }

}
