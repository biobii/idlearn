<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * Define guard as admin.
     */
    protected $guard = 'admin';

    protected $primaryKey = 'nip';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip', 'name', 'email', 'password', 'avatar', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    /**
     * Generate api_token.
     * @return $this->api_token
     */
    public function generateToken()
    {
        $this->api_token = str_random(100);
        $this->save();
        return $this->api_token;
    }

    /**
     * Destroy api_token.
     * @return bool true
     */
    public function destroyToken()
    {
        if ($this->api_token) {
            $this->api_token = null;
            $this->save();
            return true;
        }
        return false;
    }

}
