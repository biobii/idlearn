<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassRoom extends Model
{
    
    use SoftDeletes;

    protected $table = 'classes';
    protected $primaryKey = 'key';
    public $incrementing = false;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'key', 'lecturer_id', 'name',
    ];

    /*-----------
     * Relation
     *-----------
     */

    public function lecturer()
    {
        return $this->belongsTo('App\Models\Lecturer', 'lecturer_id');
    }

    public function assistants()
    {
        return $this->belongsToMany('App\Models\User', 'assistant_class', 'class_key', 'user_nipd', 'key', 'nipd')
            ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'class_user', 'class_key', 'user_nipd', 'key', 'nipd')
                        ->withTimestamps();
    }

    public function lectures()
    {
        return $this->hasMany('App\Models\Lecture', 'class_key');
    }

    public function questionMultiples()
    {
        return $this->hasMany('App\Models\QuestionMultiple', 'class_key');
    }

    public function questionEssays()
    {
        return $this->hasMany('App\Models\QuestionEssay', 'class_key', 'key');
    }

    public function scoreMultiple()
    {
        return $this->hasMany('App\Models\ScoreMultipleQuestion', 'class_key', 'key');
    }

}
