<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\LecturerResetPasswordNotification;

class Lecturer extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * Define guard as lecturer.
     */
    protected $guard = 'lecturer';

    /**
     * Define table name.
     */
    protected $table = 'lecturers';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip', 'name', 'email', 'password', 'avatar', 'gender', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new LecturerResetPasswordNotification($token));
    }

    /**
     * Generate api_token.
     * @return $this->api_token
     */
    public function generateToken()
    {
        $this->api_token = str_random(100);
        $this->save();
        return $this->api_token;
    }

    /**
     * Destroy api_token.
     * @return bool true
     */
    public function destroyToken()
    {
        if ($this->api_token) {
            $this->api_token = null;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Check owner class.
     * @param string $key
     * @return bool
     */
    public function isClassOwner($key)
    {
        return $this->classrooms()->where('key', $key)->first()
                ? true : false;
    }

    /*-----------
     * Relation
     *-----------
     */

     public function classrooms()
     {
         return $this->hasMany('App\Models\ClassRoom', 'lecturer_id', 'id');
     }

     public function lectures()
     {
         return $this->hasMany('App\Models\Lecture', 'lecturer_id', 'id');
     }
}
