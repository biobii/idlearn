<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $primaryKey = 'nipd';
    public $incrementing = false;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nipd', 'name', 'email', 'password', 'gender', 'avatar', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * Generate api_token.
     * @return $this->api_token
     */
    public function generateToken()
    {
        $this->api_token = str_random(100);
        $this->save();
        return $this->api_token;
    }

    /**
     * Destroy api_token.
     * @return bool true
     */
    public function destroyToken()
    {
        if ($this->api_token) {
            $this->api_token = null;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Check following class.
     * @param string key
     * @return bool
     */
    public function isFollowingClass($key)
    {
        return $this->classrooms()->where('key', $key)->first()
                ? true : false;
    } 

    /*-----------
     * Relation
     *-----------
     */

    public function assistants()
    {
        return $this->belongsToMany('App\Models\ClassRoom', 'assistant_class', 'user_nipd', 'class_key', 'nipd', 'key')
                    ->withPivot('user_nipd', 'class_key')
                    ->withTimestamps();
    }

    public function classrooms()
    {
        return $this->belongsToMany('App\Models\ClassRoom', 'class_user', 'user_nipd', 'class_key', 'nipd', 'key')
                    ->withTimestamps();
    }

    // who create question.
    public function questionMultiples()
    {
        return $this->hasMany('App\Models\QuestionMultiple', 'user_nipd', 'id');
    }

    public function questionEssays()
    {
        return $this->hasMany('App\Models\QuestionEssay', 'user_nipd', 'id');
    }

    public function answerEssays()
    {
        return $this->hasMany('App\Models\AnswerEssay', 'user_nipd', 'id');
    }

    public function scoreMultiple()
    {
        return $this->hasMany('App\Models\ScoreMultipleQuestion', 'user_nipd', 'nipd');
    }
    
}
