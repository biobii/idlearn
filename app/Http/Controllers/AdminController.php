<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use App\Models\User;
use App\Models\Lecturer;
use App\Models\ClassRoom;
use Illuminate\Http\Request;
use App\Traits\CustomIdMessageValidation as MessageId;

class AdminController extends Controller
{
    use MessageId;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('auth:admin-api')->only('storeLecturer');
    }

    /**
     * Display lecturers and users data
     * @param void
     */
    public function index()
    {
        $classrooms = ClassRoom::orderBy('created_at', 'DESC')
            ->paginate(3);
        
        return view('admins.index', compact('classrooms'));
    }

    /**
     * Delete classroom.
     * @param string $key
     */
    public function deleteClass($key)
    {
        $classroom = ClassRoom::where('key', $key)->firstOrFail();
        $classroom->delete();

       return redirect()->route('admin.home')->with([
            'classroom' => 'Data kelas berhasil di hapus.'
        ]);
    }

    /**
     * Display all users.
     * @param void
     */
    public function users()
    {
    
        
        $users = User::orderBy('created_at', 'DESC')
            ->withTrashed()->paginate(3);
        
        return view('admins.pengguna', compact('users'));
    }

    /**
     * Display all users.
     * @param void
     */
    public function lecturers()
    {
        $lecturers = Lecturer::orderBy('id', 'DESC')
            ->withTrashed()->paginate(3);
        
       
        
        return view('admins.lecturer', compact('lecturers'));
    }

    /**
     * Displaying form edit profile.
     * @param void
     */
    public function showProfile()
    {
        $user = auth('admin')->user();

        return view('admins.profile', compact('user'));
    }

    /**
     * Update profile.
     * @param Illuminate\Http\Request $request
     */
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->only('nama'), [
            'nama' => 'required|string|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }

        $lecturer = auth('admin')->user()
            ->update(['name' => ucwords($request->nama)]);

        return redirect()->back()->with([
            'success' => 'Berhasil memperbarui profil',
        ]);
    }

    /**
     * Display form create new lecturer.
     * @param void
     */
    public function addLecturer()
    {
        return view('admins.create-lecturer');
    }

    /**
     * Store new lecturer.
     * @param Illuminate\Http\Request $request
     */
    public function storeLecturer(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'nip' => 'required|unique:lecturers|numeric',
                'name' => 'required|string|max:255',
                'gender' => 'required|string',
                'email' => 'required|email|unique:lecturers',
        ], $this->messages);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $lecturer = Lecturer::create([
            'nip' => $request->nip,
            'name' => ucwords($request->name),
            'email' => $request->email,
            'password' => Hash::make($request->nip),
            'gender' => $request->gender,
        ]);

        return response()->json([
            'message' => 'Dosen berhasil ditambahkan!'
        ], 201);
    }

    /**
     * Delete lecturer.
     * @param string $nip
     */
    public function deleteLecturer($nip)
    {
        $lecturer = Lecturer::where('nip', $nip)->firstOrFail();

        $lecturer->delete();
        
        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data dosen berhasil dinonaktifkan.'
        ]);
    }

    /**
     * Activate lecturer.
     * @param string $nip
     */
    public function activateLecturer($nip)
    {
        $lecturer = Lecturer::withTrashed()->where('nip', $nip)
            ->firstOrFail();

        $lecturer->restore();

        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data dosen berhasil diaktifkan.'
        ]);
    }

    /**
     * Display form edit lecturer.
     * @param string $nip
     */
    public function formEditLecturer($nip)
    {
        $lecturer = Lecturer::where('nip', $nip)->firstOrFail();

        return view('admins.edit-lecturer', compact('lecturer'));
    }

    /**
     * Updating lecturer.
     * @param Illuminate\Http\Request $request
     * @param string $nip
     */
    public function updateLecturer(Request $request, $nip)
    {
        $lecturer = Lecturer::where('nip', $nip)->firstOrFail();

        $validator = Validator::make($request->all(), [
                'nama' => 'required|string',
                'gender' => 'required|string',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)->withInput();
        }

        $lecturer->update([
            'name' => ucwords($request->nama),
            'password' => Hash::make($request->nip),
            'gender' => $request->gender,
        ]);

        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data dosen berhasil diubah.'
        ]);
    }

    /**
     * Delete user.
     * @param string $nipd
     */
    public function deleteuser($nipd)
    {
        $user = User::where('nipd', $nipd)->firstOrFail();

        $user->delete();
        
        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data mahasiswa berhasil dinonaktifkan.'
        ]);
    }

    /**
     * Activate user
     * @param string $nipd
     */
    public function activateUser($nipd)
    {
        $user = User::withTrashed()->where('nipd', $nipd)
            ->firstOrFail();

        $user->restore();
        
        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data mahasiswa berhasil diaktifkan.'
        ]);
    }

    /**
     * Display form edit user.
     * @param string $nipd
     */
    public function formEditUser($nipd)
    {
        $user = User::where('nipd', $nipd)->firstOrFail();

        return view('admins.edit-user', compact('user'));
    }

    /**
     * Updating user.
     * @param Illuminate\Http\Request $request
     * @param string $nipd
     */
    public function updateUser(Request $request, $nipd)
    {
        $user = User::where('nipd', $nipd)->firstOrFail();

        $validator = Validator::make($request->all(), [
                'nipd' => 'required|unique:users|numeric',
                'nama' => 'required|string|max:255',
                'gender' => 'required|string',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)->withInput();
        }

        $user->update([
            'nipd' => $request->nipd,
            'name' => ucwords($request->nama),
            'gender' => $request->gender,
        ]);

        return redirect()->route('admin.home')->with([
            'lecturer' => 'Data mahasiswa berhasil diubah.'
        ]);
    }
}
