<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use App\Models\Lecture;
use App\Models\Lecturer;
use App\Models\ClassRoom;
use Illuminate\Http\Request;
use App\Models\ScoreMultipleQuestion;
use App\Traits\CustomIdMessageValidation as MessageId;

class LecturerController extends Controller
{

    use MessageId;

    public function __construct()
    {
        $this->middleware('auth:lecturer');
        $this->middleware('class-creator')->only(
            'showClassRoom',
            'editClassRoom',
            'updateClassRoom',
            'createAssistant',
            'deleteAssistant',
            'kickUser',
            'createLecture',
            'storeLecture',
            'deleteClassRoom'
        );
    }

    /**
     * Displaying home page of dashboard.
     * @param void
     */
    public function index()
    {
        $classrooms = Auth::guard('lecturer')->user()
            ->classrooms()->get();

        return view('lecturers.index', compact('classrooms'));
    }

    /**
     * Displaying form create new classroom.
     * @param Illuminate\Http\Request $request
     */
    public function createClassRoom(Request $request)
    {
        return view('lecturers.create-class');
    }

    /**
     * Store new classroom.
     * @param Illuminate\Http\Request $request
     */
    public function storeClassRoom(Request $request)
    {
        $validator = Validator::make($request->only('nama'), [
            'nama' => 'required|string|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }

        $classroom = ClassRoom::create([
            'lecturer_id' => Auth::guard('lecturer')->user()->id,
            'name' => ucwords($request->nama),
            'key' => str_random(6),
        ]);

        return redirect()->route('lecturer.home')
            ->with(['success' => 'Kelas berhasil dibuat.']);
    }

    /**
     * Displaying form edit classroom.
     * @param string $key
     */
    public function editClassRoom($key)
    {
        $classroom = ClassRoom::where('key', $key)->firstOrFail();

        return view('lecturers.edit-class', compact('classroom'));
    }

    /**
     * Update classroom.
     * @param Illuminate\Http\Request $request
     * @param string $key
     */
    public function updateClassRoom(Request $request, $key)
    {
        $classroom = ClassRoom::where('key', $key)->firstOrFail();

        $validator = Validator::make($request->only('nama'), [
            'nama' => 'required|string|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }
        
        $classroom->update(['name' => ucwords($request->nama)]);

        return redirect()->route('lecturer.home')
                ->with(['success' => 'Kelas berhasil disimpan.']);
    }

    /**
     * Displaying spesific classroom.
     * @param string $key
     */
    public function showClassRoom($key)
    {
        $classroom = ClassRoom::with('assistants')
            ->where('key', $key)->firstOrFail();

        $lectures = Lecture::where('class_key', $key)
            ->where('lecturer_id', Auth::guard('lecturer')->user()->id)
            ->orderBy('created_at', 'ASC')->get();

        $lecture = Lecture::where('class_key', $key)
            ->where('lecturer_id', Auth::guard('lecturer')->user()->id)
            ->orderBy('created_at', 'DESC')->limit(1)->first();

        $assistants = $classroom->assistants()->where('class_key', $key)
            ->orderBy('name', 'ASC')->get();

        $values = ScoreMultipleQuestion::where('class_key', $key)
            ->orderBy('lecture_id', 'ASC')->get();

        return view('lecturers.class', compact(
            'classroom',
            'lectures',
            'lecture',
            'assistants',
            'values'
        ));
    }

    /**
     * Delete classroom.
     * @param string $key
     */
    public function deleteClassRoom($key)
    {
        $classroom = ClassRoom::where('key', $key)->first();

        $classroom->forceDelete();
        
        return redirect()->route('lecturer.home')->with([
            'success' => 'Kelas dengan kode ' .
            $key . ' berhasil dihapus!'
        ]);
    }

    /**
     * Create assistant on specfic class.
     * @param string $class_key
     * @param string $nipd
     */
    public function createAssistant($class_key, $nipd)
    {
        $user = User::where('nipd', $nipd)->firstOrFail();

        $user->assistants()->attach($class_key);
        
        return redirect()->back()->with([
            'success' => 'Asisten berhasil ditambahkan!'
        ]);
    }

    /**
     * Delete assistant
     * @param  string $class_key
     * @param  string $nipd
     */
    public function deleteAssistant($class_key, $nipd)
    {
        $classroom = ClassRoom::where('key', $class_key)->firstOrFail();

        $classroom->assistants()->detach($nipd);
        
        return redirect()->back()->with([
            'success' => 'Berhasil menghapus asisten kelas!'
        ]);
    }

    /**
     * Kick specific user.
     * @param  string $class_key
     * @param  string $nipd
     */
    public function kickUser($class_key, $nipd)
    {
        $classroom = ClassRoom::with('users')->where('key', $class_key)->first();

        if ($classroom->assistants()->where('nipd', $nipd)->first()) {
            $this->deleteAssistant($class_key, $nipd);
        }

        $classroom->users()->detach($nipd);

        return redirect()->back()
            ->with(['success' => 'Berhasil mengeluarkan siswa.']);
    }

    /**
     * Displaying form create new lecture.
     * @param string $key
     */
    public function createLecture($key)
    {
        $classroom = ClassRoom::where('key', $key)->firstOrFail();

        return view('lecturers.create-lecture', compact('classroom'));
    }

    /**
     * Store new lecture.
     * @param Illuminate\Http\Request $request
     * @param string $key
     */
    public function storeLecture(Request $request, $key)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|string|max:255',
            'deskripsi' => 'required|string',
            'file_materi' => 'required|mimes:pdf,doc,docx,ppt,pptx|max:5000',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()
                    ->withErrors($validator)->withInput();
        }

        if ($request->hasFile('file_materi')) {
            $filename = 'lectures-' . $key . '-' . str_shuffle(time()) . '.' . $request->file_materi->extension();
            $request->file_materi->storeAs('public/lectures', $filename);
            
            $lecture = Lecture::create([
                'lecturer_id' => Auth::guard('lecturer')->user()->id,
                'class_key' => $key,
                'title' => ucwords($request->judul),
                'description' => ucfirst($request->deskripsi),
                'lecture_file' => $filename,
            ]);
        }

        return redirect()->route('lecturer.showDetailClassRoom', $key)
            ->with(['success' => 'Materi berhasil diunggah.']);
    }

    /**
     * Displaying form edit profile.
     * @param void
     */
    public function showProfile()
    {
        $user = auth('lecturer')->user();

        return view('lecturers.profile', compact('user'));
    }

    /**
     * Update profile.
     * @param Illuminate\Http\Request $request
     */
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->only('nama'), [
            'nama' => 'required|string|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }

        $lecturer = auth('lecturer')->user()
            ->update(['name' => ucwords($request->nama)]);

        return redirect()->back()->with([
            'success' => 'Berhasil memperbarui profil',
        ]);
    }

}
