<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use App\Models\Lecture;
use App\Models\ClassRoom;
use Illuminate\Http\Request;
use App\Models\QuestionMultiple;
use App\Models\ScoreMultipleQuestion;
use App\Traits\CustomIdMessageValidation as MessageId;

class UserController extends Controller
{

    use MessageId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
        $this->middleware('auth:api')->only(
            'joinClass',
            'createMultipleQuestion',
            'getMultipleQuestion',
            'submitAnswersMultiple'
        );
        $this->middleware('following-class')->only(
            'showClass',
            'showAllLectures',
            'detailLecture',
            'fileLecture'
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::guard('web')->user();

        return view('users.index', compact('user'));
    }

    /**
     * Show detail current user profile.
     */
    public function profile()
    {
        $user = auth('web')->user();

        return view('users.profile', compact('user'));
    }

    /**
     * Update profile.
     * @param Illuminate\Http\Request $request
     */
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->only('nama'), [
            'nama' => 'required|string|max:255',
        ], $this->messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }

        $lecturer = auth('web')->user()
            ->update(['name' => ucwords($request->nama)]);

        return redirect()->back()->with([
            'success' => 'Berhasil memperbarui profil',
        ]);
    }

    /**
     * Show specific following class.
     * @param string $key
     */
    public function showClass($key)
    {
        $classroom = ClassRoom::with('assistants')
            ->where('key', $key)->firstOrFail();

        $lectures = Lecture::where('class_key', $key)
            ->where('lecturer_id', $classroom->lecturer->id)
            ->orderBy('created_at', 'ASC')->get();

        $lecture = Lecture::where('class_key', $key)
            ->where('lecturer_id', $classroom->lecturer->id)
            ->orderBy('created_at', 'DESC')->limit(1)->first();

        $assistants = $classroom->assistants()
            ->where('class_key', $key)->orderBy('name', 'ASC')
            ->get();

        $values = ScoreMultipleQuestion::where('class_key', $key)
            ->orderBy('lecture_id', 'ASC')->get();

        return view('users.class', compact(
            'classroom',
            'lectures',
            'lecture',
            'assistants',
            'values'
        ));
    }

    /**
     * Show all lectures in current class.
     * @param string $key
     */
    public function showAllLectures($key)
    {
        //
    }

    /**
     * Show detail lecuture in current class.
     * @param string $key
     * @param int $lecture_id
     */
    public function detailLecture($key, $lecture_id)
    {
        //
    }

    /**
     * Show lecture file in current class.
     * @param string $key
     * @param int $lecture_id
     * @param string $filename
     */
    public function fileLecture($key, $lecture_id, $filename)
    {
        //
    }

    /**
     * Joining new class.
     * @param Illuminate\Http\Request $request
     * @param Illuminate\Http\Response
     */
    public function joinClass(Request $request)
    {
        $user = Auth::guard('api')->user();
        
        $classroom = ClassRoom::where('key', $request->key)->first();

        if (! $classroom) {
            return response()->json([
                'success' => false,
                'message' => 'Kode kelas tidak ditemukan!',
            ], 404);
        }

        if ($user->classrooms()->where('class_key', $request->key)->first()) {
            return response()->json([
                'success' => false,
                'message' => 'Kamu telah bergabung di kelas ini!',
            ], 403);
        }

        $user->classrooms()->attach($request->key);

        $newclass = $user->classrooms()->where('user_nipd', $user->nipd)
            ->where('class_key', $request->key)->limit(1)->first();

        return response()->json([
            'classroom' => $newclass,
        ], 201);
    }

    /**
     * Show following classes.
     * @param void
     * @return Illuminate\Http\Response
     */
    public function followedClass()
    {
        return response()->json([
            'classes' => Auth::guard('web')->user()->classrooms
        ], 200);
    }

    /**
     * Leave specific class.
     * @param string $key
     */
    public function leaveClass($key)
    {
        $user = Auth::guard('web')->user();

        $user->classrooms()->detach($key);
        
        return redirect()->route('user.home')->with([
            'success' => 'Berhasil keluar dari kelas.'
        ]);
    }

    /**
     * Create question or essay
     * @param string $key
     */
    public function createQuestion($key)
    {
        $classroom = ClassRoom::where('key', $key)->firstOrFail();

        $lectures = $classroom->lectures()
            ->select('id', 'title', 'class_key')
            ->where('class_key', $key)->get();

        return view('users.create-question', compact(
            'classroom',
            'lectures',
            'all'
        ));
    }

    /**
     * Mass assignment multiple questions.
     */
    public function createMultipleQuestion(Request $request)
    {
        $questions = $request->all();

        if (count($questions) > 0) {
            foreach ($questions as $question) {
                QuestionMultiple::create([
                    'user_nipd' => $question['user_nipd'],
                    'class_key' => $question['class_key'],
                    'lecture_id' => $question['lecture_id'],
                    'question' => $question['question'],
                    'choice_a' => $question['choice_a'],
                    'choice_b' => $question['choice_b'],
                    'choice_c' => $question['choice_c'],
                    'choice_d' => $question['choice_d'],
                    'key' => $question['key'],
                ]);
            }

            return response()->json([
                'message' => 'Soal berhasil dikirim ke server',
                'questions' => $questions
            ], 201);
        }

        return response()->json([
            'message' => 'terjadi kesalahan saat mengirim data'
        ], 500);
    }

    public function getMultipleQuestion($key, $lecture_id)
    {
        $questions = QuestionMultiple::where('class_key', $key)
            ->where('lecture_id', $lecture_id)->get();

        return response()->json($questions, 200);
    }

    public function submitAnswersMultiple(Request $request)
    {
        $user = auth('web')->user();

        $score = ScoreMultipleQuestion::create([
                'user_nipd' => $user->nipd,
                'class_key' => $request->class_key,
                'lecture_id' => $request->lecture_id,
                'score' => $request->score,
            ]);

        return response()->json(['score' => $request->score], 201);
    }

    public function showMultipleValue()
    {
        $user = auth('web')->user();

        $scores = ScoreMultipleQuestion::where('user_nipd', $user->nipd)
            ->get();

        return view('users.multiple-value', compact('user', 'scores'));
    }

}
