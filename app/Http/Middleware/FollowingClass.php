<?php

namespace App\Http\Middleware;

use Closure;

class FollowingClass
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('web')->check()) {
            $key = $request->route('key');
            $user = auth('web')->user()->isFollowingClass($key);
            if ($user)
                return $next($request);
        }
        
        return dd('Akses ditolak!');
    }
}
