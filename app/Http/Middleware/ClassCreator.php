<?php

namespace App\Http\Middleware;

use Closure;

class ClassCreator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('lecturer')->check()) {
            $key = $request->route('key');
            $res = auth('lecturer')->user()->isClassOwner($key);
            if ($res)
                return $next($request);
        }
        
        return dd('Akses ditolak!');
    }
}
