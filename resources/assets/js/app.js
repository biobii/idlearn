
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

axios.defaults.baseURL = 'http://localhost:8000';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('user-home', require('./components/users/Home.vue'));
Vue.component('user-create-question', require('./components/users/CreateQuestions.vue'));
Vue.component('user-answer-multiple-question', require('./components/users/AnswerMultipleQuestion.vue'));
Vue.component('admin-add-lecturer', require('./components/admins/AddLecturer.vue'));

const app = new Vue({
    el: '#app'
});
