@extends('layouts.auth')

@section('content')
<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6 offset-1">
                <div class="login-content card">
                    <div class="login-form">
                        <h4>
                            <i class="fa fa-graduation-cap"></i>
                            {{ __('Mahasiswa') }}
                        </h4>
                        <hr>

                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach

                        <form method="POST" action="{{ route('user.register.submit') }}">
                            @csrf
                            <div class="form-group">
                                <label>{{ __('NIPD') }}</label>
                                <input type="text" class="form-control" placeholder="NIPD" name="nipd" value="{{ old('nipd') }}" required autofocus>
                            </div>

                            <div class="form-group">
                                <label>{{ __('Nama Lengkap') }}</label>
                                <input type="text" class="form-control" placeholder="Nama Lengkap" name="name" value="{{ old('name') }}" required autofocus>
                            </div>

                            <div class="form-group">
                                <label>{{ __('Email') }}</label>
                                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            </div>

                            <div class="form-group">
                                <label>{{ __('Password') }}</label>
                                <input type="password" class="form-control" placeholder="Password" name="password" required>
                            </div>

                            <div class="form-group">
                                <label>{{ __('Konfirmasi Password') }}</label>
                                <input type="password" class="form-control" placeholder="Password" name="password_confirmation" required>
                            </div>

                            <div class="form-group">
                                <label>{{ __('Jenis Kelamin') }}</label>
                                <select name="gender" class="form-control">
                                    <option value="Laki-laki">{{ __('Laki-laki') }}</option>
                                    <option value="Perempuan">{{ __('Perempuan') }}</option>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">
                                {{ __('Daftar') }}
                            </button>
                            <div class="register-link m-t-15 text-center">
                                <p> {{ __('Sudah punya akun ?') }} <a href="{{ route('user.login') }}">{{ __('Login Sekarang') }}</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
