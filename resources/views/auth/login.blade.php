@extends('layouts.auth')

@section('content')
<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    <div class="login-form">
                        <h4>
                            <i class="fa fa-graduation-cap"></i>
                            {{ __('Siswa') }}
                        </h4>
                        @if (old('email'))
                            <div class="alert alert-danger">
                                {{ __('Email atau password salah') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('user.login.submit') }}">
                            @csrf
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label>{{ __('Email') }}</label>
                                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label>{{ __('Password') }}</label>
                                <input type="password" class="form-control" placeholder="Password" name="password" required>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Ingat saya?') }}
                                </label>
                                <label class="pull-right">
                                    <a href="{{ route('user.password.request') }}">
                                        {{ __('Lupa password?') }}
                                    </a>
                                </label>
                            </div>

                            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">
                                {{ __('Masuk') }}
                            </button>
                            <div class="register-link m-t-15 text-center">
                                <p> {{ __('Belum punya akun ?') }} <a href="{{ route('user.register') }}">{{ __('Daftar Sekarang') }}</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
