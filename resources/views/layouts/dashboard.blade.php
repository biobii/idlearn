<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'IDLearn')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <!-- Custom CSS -->

    <link href="{{ asset('css/ela-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ela.css') }}" rel="stylesheet">
    @yield('stylesheets')
</head>
<body>
    <div id="app">

   
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">

            @yield('dashboard-nav')

            @yield('dashboard-sidebar')

            <div class="page-wrapper">
                                
                @yield('content')

            </div>
        </div>
    </div>
    <script src="{{ asset('js/ela.js') }}" defer></script>
    @yield('scripts')
</body>
</html>
