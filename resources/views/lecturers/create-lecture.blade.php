@extends('layouts.dashboard')

@section('dashboard-nav')
    @include('lecturers.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('lecturers.partials._sidebar')
@endsection

@section('content')

@component('lecturers.partials._breadcrumb')
    @slot('sub')
        {{ __('Materi Baru') }}
    @endslot

    @slot('item')
        <li class="breadcrumb-item"><a href="javascript:void(0)">{{ __('Kelas') }}</a></li>
    @endslot

    @slot('current')
        {{ $classroom->name }}
    @endslot
@endcomponent

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif
                    <form method="POST" action="{{ route('lecturer.storeLecture', $classroom->key) }}" enctype="multipart/form-data">
                      @csrf
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Judul') }}</label>
                                    <input type="text" class="form-control" name="judul" value="{{ old('judul') }}"> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Deskripsi') }}</label>
                                    <textarea style="height: 200px" name="deskripsi" class="form-control" cols="30" rows="20">{{ old('deskripsi') }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('File Materi') }}</label>
                                    <input type="file" class="form-control" name="file_materi" value="{{ old('file_materi') }}"> 
                                </div>
                            </div>
                        </div>

                      <button type="submit" class="btn btn-primary">Unggah</button>
                    </form>                    
                </div>            
            </div>
        </div>
    </div>
</div>
@endsection
