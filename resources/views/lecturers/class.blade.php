@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
	@include('lecturers.partials._nav')
@endsection

@section('dashboard-sidebar')
	@include('lecturers.partials._sidebar')
@endsection

@section('content')

@component('lecturers.partials._breadcrumb')
    @slot('sub')
        {{ $classroom->name }} - {{ $classroom->key }}
    @endslot

	@slot('item')
		<li class="breadcrumb-item"><a href="javascript:void(0)">Kelas</a></li>
	@endslot

    @slot('current')
        {{ $classroom->name }}
    @endslot
@endcomponent

<div class="container">

	<div class="row">
        <a class="btn btn-primary left-btn-dashboard" href="{{ route('lecturer.createLecture', $classroom->key) }}">
            <i class="fa fa-upload"></i> {{ __('Upload Materi') }}
        </a>
        <a class="btn btn-warning left-btn-dashboard" href="{{ route('lecturer.editClassRoom', $classroom->key) }}">
            <i class="fa fa-edit"></i> {{ __('Sunting Kelas') }}
		</a>
		
        <a class="btn btn-danger left-btn-dashboard" href="{{ route('lecturer.deleteClassRoom', $classroom->key) }}" onclick="event.preventDefault();
						document.getElementById('delete-class').submit();">
            <i class="fa fa-trash"></i> {{ __('Hapus Kelas') }}
		</a>

		<form id="delete-class" action="{{ route('lecturer.deleteClassRoom', $classroom->key) }}" method="POST" style="display:none;">
			@csrf
			@method('DELETE')
		</form>

	</div>

	<div class="card">
	    <div class="card-body">

	        <!-- Nav tabs -->
	        <ul class="nav nav-tabs" role="tablist">
	            <li class="nav-item"> 
		            <a class="nav-link active" data-toggle="tab" href="#materi" role="tab" aria-selected="false">
			            <span>
			            	<i class="ti-book"></i> Materi Terbaru
			            </span>
		            </a> 
	            </li>

	            <li class="nav-item"> 
		            <a class="nav-link" data-toggle="tab" href="#arsip" role="tab" aria-selected="false">
		            	<span>
		            		<i class="ti-archive"></i> Arsip Materi ({{ $lectures->count() }})
		            	</span>
		            </a> 
	            </li>
	            <li class="nav-item"> 
	            	<a class="nav-link show" data-toggle="tab" href="#siswa" role="tab" aria-selected="true">
		            	<span>
		            		<i class="ti-user"></i>  Siswa ({{ $classroom->users->count() }})
		            	</span>
	            	</a> 
				</li>
				<li class="nav-item"> 
	            	<a class="nav-link show" data-toggle="tab" href="#asisten" role="tab" aria-selected="true">
		            	<span>
		            		<i class="ti-face-smile"></i>  Asisten ({{ $assistants->count() }})
		            	</span>
	            	</a> 
	            </li>
				<li class="nav-item"> 
	            	<a class="nav-link show" data-toggle="tab" href="#latihan" role="tab" aria-selected="true">
		            	<span>
		            		<i class="ti-book"></i>  Latihan
		            	</span>
	            	</a> 
	            </li>
				<li class="nav-item"> 
	            	<a class="nav-link show" data-toggle="tab" href="#nilai" role="tab" aria-selected="true">
		            	<span>
		            		<i class="ti-cup"></i>  Nilai ({{ $values->count() }})
		            	</span>
	            	</a> 
	            </li>
	        </ul>
	        <!-- Tab panes -->
	        <div class="tab-content tabcontent-border">

	            <div class="tab-pane active show" id="materi" role="tabpanel">
	                <div class="p-20">
						@if (isset($lecture))
							<h4>{{ $classroom->lecturer->name }}</h4>
							<hr>
							<h4>{{ $lecture->title }}</h4>
							<small>Diunggah {{ $lecture->created_at->diffForHumans() }}</small>
							<p style="color: #455a64; line-height: 1.5em">{{ $lecture->description }}</p>
							<a class="btn btn-primary" href="{{ asset('storage/lectures/' . $lecture->lecture_file) }}" target="_blank">
								<i class="fa fa-file-pdf-o"></i>
								Download
							</a>
						@else
							<p>Belum ada materi yang diunggah</p>
						@endif
	                </div>
				</div>
				
	            <div class="tab-pane p-20" id="arsip" role="tabpanel">
					@forelse ($lectures as $item)
						<a style="color: #455a64" href="{{ asset('storage/lectures/' . $item->lecture_file) }}" target="_blank">
							{{ $loop->iteration }}. {{ $item->title }}
						</a>
						<small> - {{ $item->created_at->diffForHumans() }}</small>
						<hr>
					@empty
						<p>Belum ada materi yang diunggah</p>
					@endforelse
				</div>

	            <div class="tab-pane p-20" id="siswa" role="tabpanel">
	            	@forelse($classroom->users()->orderBy('name', 'ASC')->get() as $user)
						{{ $loop->iteration }}. {{ $user->name }}
						
						<form style="display:inline;" action="{{ route('lecturer.kickUser', [$classroom->key, $user->nipd]) }}" method="POST" >
							<button type="submit" class="btn btn-danger btn-sm pull-right ml-2">
								keluarkan
							</button>
							@csrf
							@method('DELETE')
						</form>

						@if(! $user->assistants()->where('user_nipd', $user->nipd)-> where('class_key', $classroom->key)->first())
							<form style="display:inline;" action="{{ route('lecturer.addAssistant', [$classroom->key, $user->nipd]) }}" method="POST" >
								<button type="submit" class="btn btn-primary btn-sm pull-right">
									jadikan asisten
								</button>
								@csrf
							</form>							
						@else
							<form style="display:inline;" action="{{ route('lecturer.deleteAssistant', [$classroom->key, $user->nipd]) }}" method="POST" >
								<button type="submit" class="btn btn-warning btn-sm pull-right">
									hapus asisten 
								</button>
								@csrf
								@method('DELETE')
							</form>	
						@endif
							
						<hr>
					@empty
						<p>Belum ada siswa</p>
	            	@endforelse
				</div>
				
				<div class="tab-pane p-20" id="asisten" role="tabpanel">
					@forelse ($assistants as $assistant)
						{{ $loop->iteration }}. {{ $assistant->name }}
						
						<form style="display:inline;" action="{{ route('lecturer.deleteAssistant', [$classroom->key, $user->nipd]) }}" method="POST" >
							<button type="submit" class="btn btn-warning btn-sm pull-right">
								hapus asisten 
							</button>
							@csrf
							@method('DELETE')
						</form>	
						<hr>
					@empty
						<p>Belum ada asisten di kelas ini.</p>
					@endforelse
				</div>

				<div class="tab-pane p-20" id="latihan" role="tabpanel">

				</div>

				<div class="tab-pane p-20" id="nilai" role="tabpanel">
					<table class="table">
						<thead>
							<th>No.</th>
							<th>NIPD</th>
							<th>Nama</th>
							<th>Materi</th>
							<th>Nilai</th>
							<th>Tanggal</th>
						</thead>
						<tbody>
							@foreach ($values as $value)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $value->user->nipd }}</td>
									<td>{{ $value->user->name }}</td>
									<td>{{ $value->lecture->title }}</td>
									<td>{{ $value->score }}</td>
									<td>{{ $value->created_at }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>

	        </div>
	    </div>
	</div>
</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script>
		var success = '{{ Session::get('success') }}';
		if (success !== '') {
			swal('OK!', '{{ Session::get('success') }}', 'success');
		}
	</script>
@endsection