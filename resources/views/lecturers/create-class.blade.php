@extends('layouts.dashboard')

@section('dashboard-nav')
    @include('lecturers.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('lecturers.partials._sidebar')
@endsection

@section('content')

@component('lecturers.partials._breadcrumb')
    @slot('sub')
        Kelas Baru
    @endslot

    @slot('item')
        <li class="breadcrumb-item"><a href="javascript:void(0)">Kelas</a></li>
    @endslot

    @slot('current')
        Baru
    @endslot
@endcomponent

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif
                    <form method="POST" action="{{ route('lecturer.createClassRoom') }}">
                      @csrf
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Nama Kelas</label>
                                    <input type="text" class="form-control" placeholder="Nama kelas" name="nama" value="{{ old('nama') }}"> 
                                </div>
                            </div>
                        </div>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>                    
                </div>            
            </div>
        </div>
    </div>
</div>
@endsection
