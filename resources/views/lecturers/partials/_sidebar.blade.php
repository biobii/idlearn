<div class="left-sidebar">
    <div class="scroll-sidebar">

        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-label">{{ __('Dosen') }}</li>
                <li>
                    <a  href="{{ route('lecturer.home') }}" aria-expanded="false">
                        <i class="fa fa-institution"></i>
                        <span class="hide-menu">{{ __('Kelas') }}</span>
                    </a>                    
                </li>
                <li>
                    <a  href="{{ route('lecturer.showProfile') }}" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        <span class="hide-menu">{{ __('Profil') }}</span>
                    </a>                    
                </li>
                <li>
                    <a  href="{{ route('lecturer.logout') }}" aria-expanded="false">
                        <i class="fa fa-sign-out"></i>
                        <span class="hide-menu">{{ __('Keluar') }}</span>
                    </a>                    
                </li>
            </ul>
        </nav>

    </div>
</div>