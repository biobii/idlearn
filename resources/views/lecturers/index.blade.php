@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('lecturers.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('lecturers.partials._sidebar')
@endsection

@section('content')

@component('lecturers.partials._breadcrumb')
    @slot('sub')
        Daftar Kelas
    @endslot

    @slot('current')
        Kelas
    @endslot
@endcomponent

<div class="container-fluid">

    <div class="row">
        <a class="btn btn-primary left-btn-dashboard" href="{{ route('lecturer.createClassRoom') }}">
            <i class="fa fa-university"></i> Kelas Baru
        </a>
    </div>

    <div class="row">
        @forelse ($classrooms as $classroom)
            <div class="col-md-3">
                <a href="{{ route('lecturer.showDetailClassRoom', $classroom->key) }}" title="{{ $classroom->name }}">
                    <div class="card box-class p-30">
                        <div class="media">
                            <h2 class="prefix-{{ strtolower(substr($classroom->name, 0, 1)) }}">
                                {{ substr($classroom->name, 0, 1) }}
                            </h2>
                            <h4 class="m-b-0">{{ $classroom->name }}</h4>
                        </div>
                    </div>
                </a>
            </div>
        @empty
        <p class="lead p-info-nothing">Belum ada kelas.</p>
        @endforelse
    </div>
</div>

@endsection

@section('scripts')
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script>
		var success = '{{ Session::get('success') }}';
		if (success !== '') {
			swal('OK!', '{{ Session::get('success') }}', 'success');
		}
	</script>
@endsection
