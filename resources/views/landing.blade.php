<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>IDLearn | E-Learning</title>
        {{--  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/logo.png">  --}}
        <link href="https://fonts.googleapis.com/css?family=Lato:700%7CMontserrat:400,600" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" href="{{ asset('landing/css/font-awesome.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ asset('landing/css/style.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('landing/css/animate.css') }}"/>
    </head>

    <body>
        <header id="header" class="transparent-nav">
            <div class="container">
                <div class="navbar-header">
                        <div class="navbar-brand">
                            <h2 style="padding: 8% 0; color: #fff;">IDLearn</h2>
                        </div>
                        <button class="navbar-toggle" >
                            <span></span>
                        </button>
                </div>

                <!-- Navigation -->
                <nav id="nav">
                        <ul class="main-menu nav navbar-nav navbar-right">
                            <style type="text/css">
                                .navi
                                {
                                    margin-top: 4%;
                                }
                            </style>
                            <li class="navi">
                                <a href="/">Beranda</a>
                            </li>
                            <li class="navi">
                                <a href="#1">Tentang</a>
                            </li>
                            <li class="navi">
                                <a href="{{ route('user.register') }}">Daftar</a>
                            </li>
                        </ul>
                </nav>
                <!-- /Navigation -->

            </div>
        </header>
        <!-- /Header -->

        <!-- Home -->
        <div id="home" class="hero-area">

            <!-- Backgound Image -->
            <div class="bg-image bg-parallax overlay" style="background-image:url({{ asset('landing/img/home-background.jpg') }})"></div>
            <!-- /Backgound Image -->

            <div id='1' class="home-wrapper">
                <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <h1 class="white-text">KAMI PEDULI EDUKASI!</h1>
                                <p class="lead white-text">
                                    Memanfaatkan teknologi dalam bidang edukasi.
                                    <br>
                                    Membantu pengajar dan peserta didik dalam proses pembelajaran.
                                </p>
                                <a class="main-button icon-button" href="{{ route('lecturer.login') }}">
                                    Saya Dosen
                                </a>
                                <a class="main-button icon-button" href="{{ route('user.login') }}">
                                    Saya Mahasiswa
                                </a>
                            </div>
                        </div>
                </div>
            </div>

        </div>
        <!-- /Home -->

        <!-- About -->
        <div id="about" class="section">

            <!-- container -->
            <div class="container">

                <!-- row -->
                <div class="row">

                        <div class="col-md-6">
                            <div class="section-header">
                                <h2>Tentang IDLearn</h2>
                                <p class="lead">Cara baru untuk membantu proses belajar.</p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="about-img">
                                <img src="{{ asset('landing/img/blog-post-background.jpg') }}" alt="">
                            </div>
                        </div>

                </div>
                <!-- row -->

            </div>
            <!-- container -->
        </div>
        <!-- /About -->

        <!-- Courses -->
        <div id="courses" class="section">

            <!-- container -->
            <div class="container">

                <!-- row -->
                <div class="row">
                        
                        <div class="section-header text-center">
                            
                            <a class="anchor animated bounce" name="learnmore"></a>

                            <h2>Layanan Kami</h2>
                            <p class="lead">
                                Kami memahami kebutuhan kamu.
                            </p>
                        </div>
                        
                </div>
                <!-- /row -->

                <!-- courses -->
                <div id="courses-wrapper">

                        <!-- row -->
                        <div class="row">
                            <!-- <a class="anchor" name="learnmore"></a> -->
                            <!-- single course -->
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="course">
                                    <a href="register_internship" class="course-img">
                                            <img src="{{ asset('landing/img/course05.jpg') }}" alt="">
                                            <i class="course-link-icon fa fa-link"></i>
                                    </a>
                                    <a class="course-title" href="register_internship">
                                        Kelas Online
                                    </a>
                                    <div class="course-details">
                                            <span class="course-category"></span>
                                            <span class="course-price course-free"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- /single course -->

                            <!-- single course -->
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="course">
                                    <a href="register_hub" class="course-img">
                                            <img src="{{ asset('landing/img/course02.jpg') }}" alt="">
                                            <i class="course-link-icon fa fa-link"></i>
                                    </a>
                                    <a class="course-title" href="register_hub">
                                        Mengerjakan Soal Secara Online
                                    </a>
                                    <div class="course-details">
                                            <span class="course-category"></span>
                                            <span class="course-price course-premium"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- /single course -->

                            <!-- single course -->
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="course">
                                    <a href="#" class="course-img" target="_blank">
                                            <img src="{{ asset('landing/img/course06.jpg') }}" alt="">
                                            <i class="course-link-icon fa fa-link"></i>
                                    </a>
                                    <a class="course-title" href="#" target="_blank">
                                        Belajar dengan Kekinian
                                    </a>
                                    <div class="course-details">
                                            <span class="course-category"></span>
                                            <span class="course-price course-free"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /courses -->

                <div class="row">
                        <div class="center-btn">
                            <a class="main-button icon-button" href="{{ route('user.login') }}">
                                Bergabung Sekarang!
                            </a>
                        </div>
                </div>

            </div>
            <!-- container -->

        </div>
        <!-- /Courses -->

        <!-- Call To Action -->
        <div id="cta" class="section">

            <!-- Backgound Image -->
            <div class="bg-image bg-parallax overlay" style="background-image:url({{ asset('landing/img/cta1-background.jpg') }});"></div>
            <!-- /Backgound Image -->

            <!-- container -->
            <div class="container">

                <!-- row -->
                <div class="row">

                        <div class="col-md-6">
                            <h2 class="white-text">Motivasi</h2>
                            <p class="lead white-text">
                                Imajinasi lebih penting daripada pengetahuan.
                                Tidak semua yang bisa dihitung penting dan tidak semua yang penting bisa dihitung.
                                Hidup itu seperti naik sepeda. Untuk menjaga keseimbangan Anda, Anda harus terus bergerak <b>- Albert Einstein.</b>
                            </p>
                        </div>

                </div>
                <!-- /row -->

            </div>
            <!-- /container -->

        </div>
        <!-- /Call To Action -->

        <!-- preloader -->
        <!-- <div id='preloader'><div class='preloader'></div></div> -->
        <!-- /preloader -->

    </body>
</html>
