@extends('layouts.dashboard')

@section('dashboard-nav')
    @include('users.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('users.partials._sidebar')
@endsection

@section('content')

@component('users.partials._breadcrumb')
    @slot('sub')
        {{ __('Profil') }}
    @endslot

    @slot('current')
        {{ $user->name }}
    @endslot
@endcomponent

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif

                    @if (Session::get('success'))
                        <div class="alert alert-success">
                            <p style="color:#fff">{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('user.updateProfile') }}">
                      @csrf
                      @method('PUT')

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('NIPD') }}</label>
                                    <input type="text" class="form-control" value="{{ $user->nipd }}" disabled> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Email') }}</label>
                                    <input type="email" class="form-control" value="{{ $user->email }}" disabled> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Nama') }}</label>
                                    <input type="text" class="form-control" name="nama" value="{{ old('nama')?? $user->name }}"> 
                                </div>
                            </div>
                        </div>

                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>                    
                </div>            
            </div>
        </div>
    </div>
</div>
@endsection
