<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary prefix-r">
            {{ $sub }}
        </h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            {{ $item or '' }}
            <li class="breadcrumb-item active">
                {{ $current }}
            </li>
        </ol>
    </div>
</div>