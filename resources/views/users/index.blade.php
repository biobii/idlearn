@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('users.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('users.partials._sidebar')
@endsection

@section('content')
@component('users.partials._breadcrumb')
    @slot('sub')
        {{ __('Kelas Saya') }}
    @endslot

    @slot('current')
        {{ __('Kelas') }}
    @endslot
@endcomponent

    @if (Session::get('success'))
        <div class="container-fluid">
            <div class="alert alert-secondary">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif

    <user-home token="{{ Auth::guard('web')->user()->api_token }}"></user-home>

@endsection

@section('scripts')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection