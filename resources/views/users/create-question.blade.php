@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('users.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('users.partials._sidebar')
@endsection

@section('content')
@component('users.partials._breadcrumb')
    @slot('sub')
        {{ __('Buat Soal') }}
    @endslot

    @slot('item')
        <li class="breadcrumb-item"><a href="javascript:void(0)">Kelas</a></li>
    @endslot

    @slot('current')
        {{ $classroom->name }}
    @endslot
@endcomponent

@if ($lectures->count() >0)
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card" style="margin-top:60px;padding: 40px 30px">
                    <div class="form-group">
                        <label>Pilih Materi</label>
                        <select name="materi" class="form-control">

                            @foreach ($lectures as $lecture)
                                <option value="{{ $lecture->id }}"> {{ $lecture->title }}</option>
                            @endforeach

                        </select> 
                    </div>
                    <button class="btn btn-primary btn-lg" style="margin: 10px 0">
                        <i class="fa fa-shower"></i> Pilihan Ganda
                    </button>
                    <button class="btn btn-success btn-lg" style="margin: 10px 0">
                        <i class="fa fa-road"></i> Essay
                    </button>
                </div>
            </div>
        </div>
    </div> --}}

    <user-create-question :listlectures="{{ json_encode($lectures) }}" :nipd="{{ auth()->user()->nipd }}" token="{{ auth()->user()->api_token }}"></user-create-question>
    
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card" style="margin-top:60px;padding: 40px 30px">
                    <h4>Soal No 1000</h4>
                    <hr>
                    <div class="form-group">
                        <label>Soal</label>
                        <textarea class="form-control" style="height: 100px !important;"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Pilhan A</label>
                        <input type="text" name="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Pilhan B</label>
                        <input type="text" name="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Pilhan C</label>
                        <input type="text" name="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Pilhan D</label>
                        <input type="text" name="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Kunci Jawaban</label>
                        <br>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                          <label class="form-check-label" for="inlineRadio1">A</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                          <label class="form-check-label" for="inlineRadio2">B</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                          <label class="form-check-label" for="inlineRadio1">C</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                          <label class="form-check-label" for="inlineRadio2">D</label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button class="btn btn-success">
                            <i class="fa fa-arrow-left"></i> sebelumnya
                        </button>
                        <button class="btn btn-success">
                            <i class="fa fa-arrow-right"></i> selanjutnya
                        </button>
                        <button class="btn btn-primary pull-right" title="klik apabila selesai membuat soal">
                            <i class="fa fa-paper-plane"></i> Selesai
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@else

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                Belum ada materi di kelas ini
            </div>
        </div>
    </div>

@endif

@endsection

@section('scripts')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection