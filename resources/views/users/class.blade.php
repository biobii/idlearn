@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('users.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('users.partials._sidebar')
@endsection

@section('content')
@component('users.partials._breadcrumb')
    @slot('sub')
        {{ $classroom->name }} - {{ $classroom->key }}
    @endslot

    @slot('item')
        <li class="breadcrumb-item"><a href="javascript:void(0)">Kelas</a></li>
    @endslot

    @slot('current')
        {{ $classroom->name }}
    @endslot
@endcomponent

<div class="container-fluid">

    <div class="row">

        @if (auth()->user()->assistants()->where('class_key', $classroom->key)->first())
            <a href="{{ route('user.createQuestion', $classroom->key) }}" class="btn btn-primary left-btn-dashboard">
            <i class="fa fa-plus"></i>
                Buat Soal
            </a>
        @endif

        <form action="{{ route('user.leaveClass', $classroom->key) }}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger left-btn-dashboard">
                <i class="fa fa-sign-out"></i> Keluar Kelas
            </button>
        </form>
    </div>

    <div class="card">
        <div class="card-body">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> 
                    <a class="nav-link active" data-toggle="tab" href="#materi" role="tab" aria-selected="false">
                        <span>
                            <i class="ti-book"></i> Materi Terbaru
                        </span>
                    </a> 
                </li>

                <li class="nav-item"> 
                    <a class="nav-link" data-toggle="tab" href="#arsip" role="tab" aria-selected="false">
                        <span>
                            <i class="ti-archive"></i> Arsip Materi ({{ $lectures->count() }})
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link show" data-toggle="tab" href="#siswa" role="tab" aria-selected="true">
                        <span>
                            <i class="ti-user"></i>  Siswa ({{ $classroom->users->count() }})
                        </span>
                    </a> 
                </li>
                <li class="nav-item"> 
                    <a class="nav-link show" data-toggle="tab" href="#asisten" role="tab" aria-selected="true">
                        <span>
                            <i class="ti-face-smile"></i>  Asisten ({{ $assistants->count() }})
                        </span>
                    </a> 
                </li>

                @if (!auth()->user()->assistants()->where('class_key', $classroom->key)->first())
                <li class="nav-item"> 
	            	<a class="nav-link show" data-toggle="tab" href="#latihan" role="tab" aria-selected="true">
		            	<span>
		            		<i class="ti-book"></i>  Latihan Soal
		            	</span>
	            	</a> 
                </li>
                @endif

                @if (auth()->user()->assistants()->where('class_key', $classroom->key)->first())
                    <li class="nav-item"> 
                        <a class="nav-link show" data-toggle="tab" href="#nilai" role="tab" aria-selected="true">
                            <span>
                                <i class="ti-cup"></i>  Nilai ({{ $values->count() }})
                            </span>
                        </a> 
                    </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tabcontent-border">

                <div class="tab-pane active show" id="materi" role="tabpanel">
                    <div class="p-20">
                        @if (isset($lecture))
                            <h4>{{ $classroom->lecturer->name }}</h4>
                            <hr>
                            <h4>{{ $lecture->title }}</h4>
                            <small>Diunggah {{ $lecture->created_at->diffForHumans() }}</small>
                            <p style="color: #455a64; line-height: 1.5em">{{ $lecture->description }}</p>
                            <a class="btn btn-primary" href="{{ asset('storage/lectures/' . $lecture->lecture_file) }}" target="_blank">
                                <i class="fa fa-file-pdf-o"></i>
                                Download
                            </a>
                        @else
                            <p>Belum ada materi yang diunggah</p>
                        @endif
                    </div>
                </div>
                
                <div class="tab-pane p-20" id="arsip" role="tabpanel">
                    @forelse ($lectures as $item)
                        <a style="color: #455a64" href="{{ asset('storage/lectures/' . $item->lecture_file) }}" target="_blank">
                            {{ $loop->iteration }}. {{ $item->title }}
                        </a>
                        <small> - {{ $item->created_at->diffForHumans() }}</small>
                        <hr>
                    @empty
                        <p>Belum ada materi yang diunggah</p>
                    @endforelse
                </div>

                <div class="tab-pane p-20" id="siswa" role="tabpanel">
                    @forelse($classroom->users()->orderBy('name', 'ASC')->get() as $user)
                        {{ $loop->iteration }}. {{ $user->name }}
                        <hr>
                    @empty
                        <p>Belum ada siswa</p>
                    @endforelse
                </div>
                
                <div class="tab-pane p-20" id="asisten" role="tabpanel">
                    @forelse ($assistants as $assistant)
                        {{ $loop->iteration }}. {{ $assistant->name }}
                        <hr>
                    @empty
                        <p>Belum ada asisten di kelas ini.</p>
                    @endforelse
                </div>

                @if (!auth()->user()->assistants()->where('class_key', $classroom->key)->first())
                    <div class="tab-pane p-20" id="latihan" role="tabpanel">
                        @if (isset($lecture))
                            <h5>Latihan: <i>"{{ $lecture->title }}"</i></h5>
                            <hr>
                            @if (auth('web')->user()->scoreMultiple()->where('lecture_id', $lecture->id)->first())
                                <p>Kamu telah mengerjakan soal latihan, dengan skor {{ auth('web')->user()->scoreMultiple()->where('lecture_id', $lecture->id)->first()->score }}</p>
                            @else
                                <user-answer-multiple-question token="{{ auth('web')->user()->api_token }}" class_key="{{ $classroom->key }}" lecture_id="{{ $lecture->id }}"></user-answer-multiple-question>
                            @endif
                        @else
                            <p>Tidak ada latihan</p>
                        @endif
                    </div>
                @endif

                @if (auth()->user()->assistants()->where('class_key', $classroom->key)->first())
                    <div class="tab-pane p-20" id="nilai" role="tabpanel">
                        <table class="table">
                            <thead>
                                <th>No.</th>
                                <th>NIPD</th>
                                <th>Nama</th>
                                <th>Materi</th>
                                <th>Nilai</th>
                                <th>Tanggal</th>
                            </thead>
                            <tbody>
                                @foreach ($values as $value)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $value->user->nipd }}</td>
                                        <td>{{ $value->user->name }}</td>
                                        <td>{{ $value->lecture->title }}</td>
                                        <td>{{ $value->score }}</td>
                                        <td>{{ $value->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection