@extends('layouts.dashboard')

@section('dashboard-nav')
    @include('users.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('users.partials._sidebar')
@endsection

@section('content')
@component('users.partials._breadcrumb')
    @slot('sub')
        Beranda
    @endslot

    @slot('current')
        Nilai
    @endslot
@endcomponent

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>{{ auth('web')->user()->name }}</h3>
                    @if ($scores)
                        <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Kode Kelas</th>
                                <th>Nama Kelas</th>
                                <th>Materi</th>
                                <th>Nilai</th>
                                <th>Tanggal</th>
                            </thead>
                            <tbody>
                                @foreach ($scores as $score)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $score->classroom->key }}</td>
                                        <td>{{ $score->classroom->name }}</td>
                                        <td>{{ $score->lecture->title }}</td>
                                        <td>{{ $score->score }}</td>
                                        <td>{{ $score->created_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>Belum ada nilai apapun.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection