@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
        
        <div class="col-md-12">
            <div class="card">
                <h4>Mahasiswa</h4>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>NIPD</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Email</th>
                            <th colspan="2">Aksi</th>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>        
                                    <td>{{ $user->nipd }}</td>        
                                    <td>{{ $user->name }}</td>        
                                    <td>{{ $user->gender }}</td>        
                                    <td>{{ $user->email }}</td>        
                                    <td>
                                        @if ($user->deleted_at != null)
                                            <form method="POST" action="{{ route('admin.activateUser', $user->nipd) }}">
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    Aktifkan
                                                </button>
                                            </form>
                                        @else
                                            <form method="POST" action="{{ route('admin.deleteuser', $user->nipd) }}">
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    Hapus
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($user->deleted_at == null)
                                           <a href="{{ route('admin.formEditUser', $user->nipd) }}" class="btn btn-sm btn-primary">
                                               Edit
                                           </a>
                                        @endif
                                    </td>   
                                </tr>
                            @endforeach
                        </tbody>                       
                    </table>
                    <hr>
                     {!! $users->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection