@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
    <div class="col-md-12">
        @if (Session::get('classroom'))
            <div class="alert alert-success">
                {{ Session::get('classroom') }}
            </div>
        @endif

        <div class="card mt-5">
            <h4>Daftar Kelas</h4>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <th>Kede Kelas</th>
                        <th>Kelas</th>
                        <th>Dosen Pengampu</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach ($classrooms as $classroom)
                        <tr>
                            <td>{{ $classroom->key }}</td>
                            <td>{{ $classroom->name }}</td>
                            <td>{{ $classroom->lecturer->name }}</td>
                            <td>
                                <form method="POST" action="{{ route('admin.deleteClassRoom', $classroom->key ) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger" type="submit">
                                    Hapus
                                </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $classrooms->links() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">

</script>

@endsection
