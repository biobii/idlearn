@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4>Sunting Akun Dosen</h4>
                <hr>
                <div class="card-body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('admin.updateLecturer', $lecturer->nip)  }}">
                      @csrf
                      @method('PUT')
                      <div class="form-group">
                        <label for="exampleInputEmail1">NIP</label>
                        <input type="text" class="form-control" placeholder="NIP" name="nip" value="{{ old('nip')?? $lecturer->nip }}" disabled>                      
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{ old('nama')?? $lecturer->name }}">                      
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <select class="form-control" name="gender" value="{{ old('gender')?? $lecturer->gender }}">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>                     
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" placeholder="Enter email" name="email" value="{{ $lecturer->email }}" disabled>                      
                      </div>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>                    
                </div>
            </div>
        </div>

       
    </div>
</div>
@endsection
