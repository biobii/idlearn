@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h4>Akun Dosen</h4>
                    <hr>

                    <admin-add-lecturer token="{{ auth('admin')->user()->api_token }}"></admin-add-lecturer>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/converter.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
