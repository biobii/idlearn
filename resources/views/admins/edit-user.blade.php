@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h4>Sunting Akun Mahasiswa</h4>
                <hr>
                <div class="card-body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                   <form method="POST" action="{{ route('admin.updateUser', $user->nipd) }}">
                     @csrf
                      @method('PUT')
                      <div class="form-group">
                        <label for="exampleInputEmail1">NIPD</label>
                        <input type="text" class="form-control" name="nipd" value="{{ old('nipd')?? $user->nipd }}" disabled>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" name="nama" value="{{ old('nama')?? $user->name }}">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" class="form-control" name="email" value="{{ old('nipd')?? $user->email }}" disabled>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <select class="form-control" name="gender" value="{{ old('gender')?? $user->gender }}">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>                     
                      </div>
                       <button type="submit" class="btn btn-primary">Submit</button>
                   </form>                  
                </div>
            </div>
        </div>

       
    </div>
</div>
@endsection
