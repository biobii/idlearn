@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
        <div class="col-md-12">

            @if (Session::get('lecturer'))
                <div class="alert alert-success">
                    {{ Session::get('lecturer') }}
                </div>
            @endif

            <div class="card">
                <h4>Dosen</h4> 
                <div class="mt-1 mb-3 float-right">
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.addLecturer') }}">Tambah Akun Dosen</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th colspan="2">Aksi</th>
                        </thead>
                        <tbody>
                            @foreach($lecturers as $lecturer)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>        
                                    <td>{{ $lecturer->nip }}</td>        
                                    <td>{{ $lecturer->name }}</td>        
                                    <td>{{ $lecturer->gender }}</td>        
                                    <td>{{ $lecturer->email }}</td>     
                                    <td>
                                        @if ($lecturer->deleted_at != null)
                                            <label class="badge badge-danger">Banned</label>
                                        @else
                                            <badge class="badge badge-success">Aktif</badge>
                                        @endif
                                    </td>   
                                    <td>
                                        @if ($lecturer->deleted_at != null)
                                            <form method="POST" action="{{ route('admin.activateLecturer', $lecturer->nip ) }}">
                                            @csrf
                                            <button class="btn btn-sm btn-success" type="submit">
                                                Aktifkan
                                            </button>
                                            </form>
                                        @else
                                            <form method="POST" action="{{ route('admin.deleteLecturer', $lecturer->nip ) }}">
                                            @csrf
                                            <button class="btn btn-sm btn-danger" type="submit">
                                                Hapus
                                            </button>
                                            </form>
                                        @endif
                                    </td>     
                                    <td>
                                        @if ($lecturer->deleted_at == null)
                                            <a href="{{ route('admin.formEditLecturer', $lecturer->nip) }}" class="btn btn-sm btn-primary">
                                                Edit
                                            </a>
                                        @endif
                                    </td>   
                                </tr>
                            @endforeach
                        </tbody>                       
                    </table>
                    <hr>
                     {!! $lecturers->links() !!}

                </div>
            </div>
        
    </div>
</div>
@endsection