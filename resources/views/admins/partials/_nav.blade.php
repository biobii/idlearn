
<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">
                IDLearn
            </a>
        </div>
        
        <div class="navbar-collapse">
            
            <ul class="navbar-nav mr-auto mt-md-0">
                
            </ul>

            <ul class="navbar-nav my-lg-0">
                <li class="nav-item">
                    <p class="lead">Hai, {{ auth('admin')->user()->name }}</p>
                </li>
            </ul>
        </div>
    </nav>
</div>