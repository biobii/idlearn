<div class="left-sidebar">
    <div class="scroll-sidebar">

        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-label">{{ __('Administrator') }}</li>
                
                <li>
                    <a  href="{{ route('admin.home') }}" aria-expanded="false">
                        <i class="fa fa-institution"></i>
                        <span class="hide-menu">{{ __('Kelas') }}</span>
                    </a>                    
                </li>
                
                <li>
                    <a  href="{{ route('admin.lecturer') }}" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                        <span class="hide-menu">{{ __('Dosen') }}</span>
                    </a>                    
                </li>

                <li>
                    <a  href="{{ route('admin.users') }}" aria-expanded="false">
                        <i class="fa fa-mortar-board"></i>
                        <span class="hide-menu">{{ __('Mahasiswa') }}</span>
                    </a>                    
                </li>


                <li>
                    <a  href="{{ route('admin.showProfile') }}" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        <span class="hide-menu">{{ __('Profil') }}</span>
                    </a>                    
                </li>
                
                <li>
                    <a  href="{{ route('admin.logout') }}" aria-expanded="false">
                        <i class="fa fa-sign-out"></i>
                        <span class="hide-menu">{{ __('Keluar') }}</span>
                    </a>                    
                </li>
            </ul>
        </nav>

    </div>
</div>