@extends('layouts.dashboard')

@section('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('dashboard-nav')
    @include('admins.partials._nav')
@endsection

@section('dashboard-sidebar')
    @include('admins.partials._sidebar')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif

                    @if (Session::get('success'))
                        <div class="alert alert-success">
                            <p style="color:#fff">{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('admin.updateProfile') }}">
                      @csrf
                      @method('PUT')

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('NIP') }}</label>
                                    <input type="text" class="form-control" value="{{ $user->nip }}" disabled> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Email') }}</label>
                                    <input type="email" class="form-control" value="{{ $user->email }}" disabled> 
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>{{ __('Nama') }}</label>
                                    <input type="text" class="form-control" name="nama" value="{{ old('nama')?? $user->name }}"> 
                                </div>
                            </div>
                        </div>

                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>                    
                </div>            
            </div>
        </div>
    </div>
</div>


@endsection