<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClassRoomTest extends TestCase
{
    
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->classroom = factory('App\Models\ClassRoom')->make();
    }

    /** @test */
    public function guestCantCreateClassRoom()
    {
        $this->get('/d/kelas-baru')
            ->assertRedirect('/auth/d/masuk');

        $this->post('d/kelas-baru', $this->classroom->toArray())
            ->assertRedirect('/auth/d/masuk');
    }

    /** @test */
    public function adminCantCreateClassRoom()
    {
        $admin = factory('App\Models\Admin')->make();
        $this->actingAs($admin);

        $this->get('/d/kelas-baru')
            ->assertRedirect('/auth/d/masuk');

        $this->post('d/kelas-baru', $this->classroom->toArray())
            ->assertRedirect('/auth/d/masuk');
    }

    /** @test */
    public function userCantCreateClassRoom()
    {
        $user = factory('App\Models\User')->make();
        $this->actingAs($user);

        $this->get('/d/kelas-baru')
            ->assertRedirect('/auth/d/masuk');

        $this->post('d/kelas-baru', $this->classroom->toArray())
            ->assertRedirect('/auth/d/masuk');
    }

    /** @test */
    public function lecturerCanCreateClassRoom()
    {
        $lecturer = factory('App\Models\Lecturer')->make();
        $this->actingAs($lecturer, 'lecturer');

        $this->post('d/kelas-baru', $this->classroom->toArray())
        ->assertRedirect('/');
    }

    /** @test */
    public function onlyLecturerCanEditClassRoom()
    {
        $slug = 'd/kelas-edit/' . $this->classroom->key;

        $admin = factory('App\Models\Admin')->make();
        $this->actingAs($admin);

        $this->get($slug)
            ->assertRedirect('auth/d/masuk');

        $user = factory('App\Models\User')->make();
        $this->actingAs($user);

        $this->get($slug)
            ->assertRedirect('auth/d/masuk');

        $lecturer = factory('App\Models\Lecturer')->make();
        $this->actingAs($lecturer);

        $this->get($slug);

        $newName = 'Updating Class Name';
        $this->json('PUT', $slug, [
            'name' => $newName,
        ]);
    }
}
