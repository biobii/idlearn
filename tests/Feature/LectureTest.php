<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LectureTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->lecture = factory('App\Models\Lecture')->create();
        $this->lecturer = factory('App\Models\Lecturer')->create();
        $this->classroom = factory('App\Models\ClassRoom')->create();
    }

    /** @test */
    public function lecturerCanUploadLecture()
    {
        // $this->actingAs($this->lecturer);
        // $this->post('/d/kelas/' . $this->classroom->key
        //         . '/materi-baru', $this->lecture->toArray())
        //         ->assertRedirect('/d/kelas/' . $this->classroom->key);
    }

}
