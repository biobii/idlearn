<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionMultiplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_multiples', function (Blueprint $table) {
            $table->increments('id');
            $table->char('user_nipd', 9);
            $table->char('class_key', 6);
            $table->unsignedInteger('lecture_id');
            $table->text('question');
            $table->string('choice_a');
            $table->string('choice_b');
            $table->string('choice_c');
            $table->string('choice_d');
            $table->char('key', 1);
            $table->timestamps();

            $table->foreign('user_nipd')->references('nipd')
                    ->on('users')->onDelete('cascade');
            $table->foreign('class_key')->references('key')
                    ->on('classes')->onDelete('cascade');
            $table->foreign('lecture_id')->references('id')
                    ->on('lectures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_multiples');
    }
}
