<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lecturer_id');
            $table->char('class_key', 6);
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('lecture_file')->unique();
            $table->timestamps();

            $table->foreign('lecturer_id')->references('id')
                    ->on('lecturers')->onDelete('cascade');
            $table->foreign('class_key')->references('key')
                    ->on('classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures');
    }
}
