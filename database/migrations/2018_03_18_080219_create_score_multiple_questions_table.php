<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreMultipleQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_multiple_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->char('user_nipd', 9);
            $table->char('class_key', 6);
            $table->unsignedInteger('lecture_id');
            $table->integer('score');
            $table->timestamps();

            $table->foreign('user_nipd')->references('nipd')
                ->on('users')->onDelete('cascade');
            $table->foreign('class_key')->references('key')
                ->on('classes')->onDelete('cascade');
            $table->foreign('lecture_id')->references('id')
                ->on('lectures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_multiple_questions');
    }
}
