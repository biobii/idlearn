<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerEssaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_essays', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_essay_id');
            $table->char('user_nipd', 9);
            $table->unsignedInteger('lecture_id');
            $table->text('answer');
            $table->integer('point')->default(0);
            $table->timestamps();

            $table->foreign('question_essay_id')->references('id')
                    ->on('question_essays')->onDelete('cascade');
            $table->foreign('user_nipd')->references('nipd')
                    ->on('users')->onDelete('cascade');
            $table->foreign('lecture_id')->references('id')
                    ->on('lectures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_essays');
    }
}
