<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistantClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_class', function (Blueprint $table) {
            $table->char('user_nipd', 9);
            $table->char('class_key', 6);
            $table->timestamps();

            $table->foreign('user_nipd')->references('nipd')
                    ->on('users')->onDelete('cascade');
            $table->foreign('class_key')->references('key')
                    ->on('classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistent_class');
    }
}
