<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Lecturer::class, function (Faker $faker) {
    $nip = '625372549073627193';
    $gender = ['Laki-laki', 'Perempuan'];
    return [
        'nip' => str_shuffle($nip),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
    	'gender' => $gender[rand(0, 1)],
    ];
});
