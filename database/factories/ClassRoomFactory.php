<?php

use App\Models\Lecturer;
use Faker\Generator as Faker;

$factory->define(App\Models\ClassRoom::class, function (Faker $faker) {
    return [
        'lecturer_id' => Lecturer::all()->random()->id,
        'name' => $faker->words(rand(1,3), true),
        'key' => str_random(6),
    ];
});
