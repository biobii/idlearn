<?php

use App\Models\Lecturer;
use App\Models\ClassRoom;
use Faker\Generator as Faker;

$factory->define(App\Models\Lecture::class, function (Faker $faker) {
    $random = 'hgsd6as3sadg';
    return [
        'lecturer_id' => Lecturer::all()->random()->id,
        'class_key' => ClassRoom::all()->random()->key,
        'title' => $faker->words(4, true),
        'description' => $faker->paragraph(rand(1,3)),
        'lecture_file' => 'lecture_' . str_shuffle($random),
    ];
});
