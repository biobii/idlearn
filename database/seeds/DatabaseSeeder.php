<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(DosenSeeder::class);
        // factory(App\Models\Lecturer::class, 9)->create();
        // factory(App\Models\User::class, 10)->create();
        // factory(App\Models\ClassRoom::class, 20)->create();
        $this->call(UserSeeder::class);
        // factory(App\Models\Lecture::class, 50)->create();
        // $this->call(AssistantSeeder::class);
        // $this->call(QuestionMultipleSeeder::class);
        // $this->call(ScoreMultipleQuestionSeeder::class);
        // $this->call(QuestionEssaySeeder::class);
        // $this->call(AnswerEssaySeeder::class);
    }
}
