<?php

use App\Models\User;
use App\Models\ClassRoom;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $faker = Faker::create();
        // $gender = ['Laki-laki', 'Perempuan'];
        // $number = '142563762';
        // $classroom = [];

        // for ($i = 0; $i < 3; $i++) {
        //     $classroom[] = (string) ClassRoom::all()->random()->key;
        // }

        // for ($i = 0; $i < 10; $i++) {
        //     $user = User::create([
        //         'nipd' => str_shuffle($number),
        //         'email' => $faker->unique()->safeEmail,
        //         'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        //         'remember_token' => str_random(10),
        //         'name' => $faker->name,
        //         'gender' => $gender[rand(0, 1)],
        //     ]);
        //     $user->classrooms()->sync($classroom, false);
        // }

        User::create([
            'nipd' => '123456789',
            'email' => 'aldi@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(10),
            'name' => 'Aldi Rahman',
            'gender' => 'Laki-laki',
        ]);

        User::create([
            'nipd' => '123456780',
            'email' => 'sabrina@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(10),
            'name' => 'Sabrina',
            'gender' => 'Perempuan',
        ]);

        User::create([
            'nipd' => '123456781',
            'email' => 'ryan@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(10),
            'name' => 'Ryan Winy',
            'gender' => 'Laki-laki',
        ]);

        User::create([
            'nipd' => '123456782',
            'email' => 'dina@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'remember_token' => str_random(10),
            'name' => 'Dina Risky',
            'gender' => 'Perempuan',
        ]);
    }
}
