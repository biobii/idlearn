<?php

use App\Models\Lecturer;
use Illuminate\Database\Seeder;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = ['Laki-laki', 'Perempuan'];
        Lecturer::create([
            'nip' => '829382738172680127',
            'name' => 'Budiman',
            'email' => 'budiman@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'gender' => $gender[0],
        ]);

        Lecturer::create([
            'nip' => '629382738172680129',
            'name' => 'Dini Rahmawati',
            'email' => 'dini@idlearn.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
            'gender' => $gender[1],
        ]);
    }
}
