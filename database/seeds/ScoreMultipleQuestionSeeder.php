<?php

use App\Models\User;
use App\Models\ClassRoom;
use Illuminate\Database\Seeder;
use App\Models\ScoreMultipleQuestion;

class ScoreMultipleQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $score = [1, 0];
        for ($i=1; $i <= 10; $i++) {
            ScoreMultipleQuestion::create([
                'user_nipd' => User::all()->random()->nipd,
                'class_key' => ClassRoom::all()->random()->key, 
                'lecture_id' => 1, 
                'score' => $score[rand(0,1)],
            ]);
        }
        
    }
}
