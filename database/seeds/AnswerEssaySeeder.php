<?php

use App\Models\User;
use Faker\Factory as Faker;
use App\Models\AnswerEssay;
use Illuminate\Database\Seeder;

class AnswerEssaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $point = [10, 5, 0];
        $faker = Faker::create();
        
        for ($i=1; $i <= 10; $i++) { 
            AnswerEssay::create([
                'question_essay_id' => $i, 
                'user_nipd' => User::all()->random()->nipd, 
                'lecture_id' => 1,
                'answer' => $faker->words(5, true), 
                'point' => $point[rand(0,2)],
            ]);
        }
    }
}
