<?php

use App\Models\User;
use App\Models\ClassRoom;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\QuestionMultiple;

class QuestionMultipleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key = ['a', 'b', 'c', 'd'];
        $faker = Faker::create();
        for ($i = 1; $i < 20; $i++) {
            QuestionMultiple::create([
                'user_nipd' => User::all()->random()->nipd,
                'class_key' => ClassRoom::all()->random()->key,
                'lecture_id' => 1,
                'question' => $faker->words(6, true),
                'choice_a' => $faker->words(1, true),
                'choice_b' => $faker->words(1, true),
                'choice_c' => $faker->words(1, true),
                'choice_d' => $faker->words(1, true),
                'key' => $key[rand(0, 3)],
            ]);
        }
    }
}
