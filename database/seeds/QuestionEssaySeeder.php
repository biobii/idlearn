<?php

use App\Models\User;
use App\Models\ClassRoom;
use Faker\Factory as Faker;
use App\Models\QuestionEssay;
use Illuminate\Database\Seeder;

class QuestionEssaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=1; $i<= 10 ; $i++) {
            QuestionEssay::create([
                'user_nipd' => User::all()->random()->nipd, 
                'class_key' => ClassRoom::all()->random()->key, 
                'lecture_id' => 1, 
                'question' => $faker->words(10, true),
            ]);
        }
    }
}
