<?php

use App\Models\User;
use App\Models\ClassRoom;
use Illuminate\Database\Seeder;

class AssistantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 6; $i++) {
            $user = User::all()->random();
            $class = ClassRoom::all()->random()->key;
        }
        $user->assistants()->sync($class, false);
    }
}
